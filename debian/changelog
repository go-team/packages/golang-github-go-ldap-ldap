golang-github-go-ldap-ldap (3.4.9-1) unstable; urgency=medium

  * New upstream release
    - Refresh patch to skip tests that attempt network access

 -- Mathias Gibbens <gibmat@debian.org>  Thu, 19 Dec 2024 14:27:20 +0000

golang-github-go-ldap-ldap (3.4.8-1) unstable; urgency=medium

  * New upstream release
  * d/control:
    - Update Standards-Version to 4.7.0 (no changes needed)
    - Update Build-Depends and Depends
  * Update years in d/copyright

 -- Mathias Gibbens <gibmat@debian.org>  Sat, 20 Apr 2024 21:25:31 +0000

golang-github-go-ldap-ldap (3.4.6-1) unstable; urgency=medium

  * New upstream release
    - Refresh patch to skip tests that attempt network access
  * Update Build-Depends and Depends in d/control

 -- Mathias Gibbens <gibmat@debian.org>  Sat, 11 Nov 2023 15:28:08 +0000

golang-github-go-ldap-ldap (3.4.5-1) unstable; urgency=medium

  * New upstream release
  * d/control:
    - Update my email address
    - Update Standards-Version to 4.6.2 (no changes needed)
    - Add Breaks for golang-github-canonical-candid-dev <= 1.12.2-1
  * Update years in d/copyright

 -- Mathias Gibbens <gibmat@debian.org>  Fri, 16 Jun 2023 21:54:55 +0000

golang-github-go-ldap-ldap (3.4.4-2) unstable; urgency=medium

  * Team upload.
  * Build-Depends: golang-github-stretchr-testify-dev (>= 1.7.2~),
    Depends: golang-golang-x-crypto-dev

 -- Drew Parsons <dparsons@debian.org>  Thu, 10 Nov 2022 17:37:39 +0100

golang-github-go-ldap-ldap (3.4.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - versioned Depends: golang-gopkg-asn1-ber.v1-dev (>= 1.5.4~),
      golang-github-azure-go-ntlmssp-dev (>= 0.0~git20220621.cb9428e~)
  * Standards-Version: 4.6.1

 -- Drew Parsons <dparsons@debian.org>  Thu, 10 Nov 2022 15:14:44 +0100

golang-github-go-ldap-ldap (3.4.3-1) unstable; urgency=medium

  [ Aloïs Micard ]
  * update debian/gitlab-ci.yml (using pkg-go-tools/ci-config)

  [ Mathias Gibbens ]
  * New upstream release
    - Refresh patch
  * Update years in d/copyright

 -- Mathias Gibbens <mathias@calenhad.com>  Fri, 29 Apr 2022 00:46:37 +0000

golang-github-go-ldap-ldap (3.4.1-1) unstable; urgency=medium

  [ Mathias Gibbens ]
  * New upstream release
  * d/control
    - Add myself to Uploaders, and remove Alexandre Viau (Closes: #940360)
        Thanks for your work Alexandre!
    - Update Go Team's email address in Maintainer field
    - Update Section to golang
    - Remove version constraint on golang-gopkg-asn1-ber.v1-dev, as it's
      no longer needed for bookworm
    - Bump Standards-Version (no changes needed)
    - Add Rules-Requires-Root: no
    - Add Multi-Arch: foreign
  * Add myself to d/copyright
  * Refresh d/rules
  * Update d/watch

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

 -- Mathias Gibbens <mathias@calenhad.com>  Fri, 26 Nov 2021 11:41:40 +0100

golang-github-go-ldap-ldap (3.2.3-1) unstable; urgency=medium

  * New upstream version.
  * Refresh patches.
  * Depend on golang-github-azure-go-ntlmssp-dev.
  * Depend on ber 1.5.1.

 -- Alexandre Viau <aviau@debian.org>  Sat, 29 Aug 2020 23:20:51 -0400

golang-github-go-ldap-ldap (2.5.1-4) unstable; urgency=medium

  * Team upload.
  * Vcs-* urls: pkg-go-team -> go-team.

 -- Alexandre Viau <aviau@debian.org>  Mon, 05 Feb 2018 23:58:38 -0500

golang-github-go-ldap-ldap (2.5.1-3) unstable; urgency=medium

  * point Vcs-* urls to salsa.d.o subproject

 -- Alexandre Viau <aviau@debian.org>  Thu, 25 Jan 2018 16:36:13 -0500

golang-github-go-ldap-ldap (2.5.1-2) unstable; urgency=medium

  * Move to salsa.debian.org.

 -- Alexandre Viau <aviau@debian.org>  Sat, 30 Dec 2017 13:38:11 -0500

golang-github-go-ldap-ldap (2.5.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.5.1
    - New patch: Require explicit intention for empty password.
      This is a cherry-pick of 95ede12 from upstream, which fixes
      CVE-2017-14623. (Closes: #876404)
  * Use debhelper v10
  * Update team name
  * Update to Standards-Version 4.1.1
    - Use HTTPS URL for d/copyright
    - Use Priority: optional
  * Use golang-any instead of golang-go
  * Update d/copyright
  * Use wrap-and-sort for d/control

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 29 Nov 2017 14:09:11 +0100

golang-github-go-ldap-ldap (2.5.0-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: adapt to BSD -> Expat license change.
  * Bump Standards-Version to 4.0.0.
  * Set Testsuite to autopkgtest-pkg-go.

 -- Alexandre Viau <aviau@debian.org>  Wed, 02 Aug 2017 16:55:53 -0400

golang-github-go-ldap-ldap (2.4.1-1) unstable; urgency=medium

  * New upstream version.
  * Bump Standards-Version to 3.9.8.

 -- Alexandre Viau <aviau@debian.org>  Tue, 16 Aug 2016 12:19:35 -0400

golang-github-go-ldap-ldap (2.3.0-1) unstable; urgency=medium

  * New upstream version
  * Added watch file
  * Refreshed disable-internet-tests.patch
  * Replaced Vcs-Git by secure (https) url
  * Make use of XS-Go-Import-Path
  * Changed my email to @debian.org
  * Bumped Standards-Version to 3.9.7
  * New /usr/share/gocode/src/gopkg.in/ldap.v2 symlink

 -- Alexandre Viau <aviau@debian.org>  Mon, 21 Mar 2016 23:52:07 -0400

golang-github-go-ldap-ldap (0.0~git20150817.24.12f2865-1) unstable; urgency=medium

  * Initial release (Closes: #798022)

 -- Alexandre Viau <alexandre@alexandreviau.net>  Wed, 02 Sep 2015 21:13:23 -0400
